// Credit hard exercise | Week 1
#include <cs50.h>
#include <stdio.h>

int count_digits(long n);
int get_first_2_digits(long input);
bool luhn_algorithm(long t);

int main(void)
{
    long n;
    int first_digit, first_2_digits, r_luhn_algorith, digits;

    // Request for input > 1
    do
    {
        n = get_long("Number: ");
    }
    while (n < 1);

    // Validate if the number verify the Luhn Algorithm 
    r_luhn_algorith = luhn_algorithm(n);
    if (r_luhn_algorith != true)
    {
        printf("INVALID\n");
        return 0;
    }

    // Count how match digits are in the number
    digits = count_digits(n);

    // Get first Digit using get_first_2_digits function
    first_digit = get_first_2_digits(n) / 10;
    first_2_digits = get_first_2_digits(n);

    // Validate if the number is for MasterCard, Visa, Amex or Invalid.
    if (digits == 16 && (first_2_digits > 50 && first_2_digits < 56))
    {
        printf("MASTERCARD\n");
    }
    else if ((digits == 16 || digits == 4) && first_digit == 4)
    {
        printf("VISA\n");
    }
    else if (digits == 15 && (first_2_digits == 34 || first_2_digits == 37))
    {
        printf("AMEX\n");
    }  
    else
    {
        printf("INVALID\n");
    }   
}

// Functions 
int count_digits(long n)
{
    int count = 0;
    do
    {
        // Increment digit count
        count ++;
        // Remove last digit
        n /= 10;
    } 
    while (n != 0);
    return count;
}

int get_first_2_digits(long input)
{
    while (input >= 100) 
    {
        input /= 10;
    }
    return input;
}

bool luhn_algorithm(long t)
{
    int remainder, count = 0, summary_even = 0, summary_odd = 0;
    while (t != 0)
    {
        count ++;
        remainder = t % 10;
        t         /= 10;
        if (count % 2 != 0)
        {
            summary_even += remainder;
        }
        else
        {
            if (remainder * 2 >= 10)
            {
                // Sum odd two digits
                summary_odd = summary_odd + (remainder * 2) % 10 + (remainder * 2) / 10;
            }       
            else
            {
                summary_odd += remainder * 2;
            }
        }
    }
    // The function needs to return 0 to verified the Luhn's Algorithm
    return ((summary_even + summary_odd) % 10) == 0 ? true : false;
}

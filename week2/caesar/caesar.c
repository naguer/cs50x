// Caesar exercise | Week 2
#include <stdio.h>
#include <cs50.h>
#include <stdlib.h>
#include <ctype.h>

int main(int argc, char *argv[])
{
    // Validate arguments inputs, only accept 1 extra argument
    if (argc != 2)
    {
        printf("Usage: ./caesar key\n");
        return 1;
    }
    
    // Validate if argument input is a number
    for (int i = 0; argv[1][i] != '\0'; i++)
    {
        if (!isdigit(argv[1][i]))
        {
            printf("Usage: ./caesar key\n");
            return 1;
        }
    }
    
    // Convert char input to integer, 'k' is the encryption key
    int k = atoi(argv[1]);
    string plaintext = get_string("plaintext: ");

    // Caesar’s algorithm
    for (int i = 0; plaintext[i] != 0; i++)
    {
        if (islower(plaintext[i]))
        {
            plaintext[i] = ((plaintext[i] + k - 'a') % 26) + 'a';
        }
        else if (isupper(plaintext[i]))
        {
            plaintext[i] = ((plaintext[i] + k - 'A') % 26) + 'A';
        }
    }
    printf("ciphertext: %s\n", plaintext);
    return 0;
}
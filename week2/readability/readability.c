// Readability exercise | Week 2
#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

void calculate_results(int size, int *results, const char *text);

// Array Size, for store the letters, words and setences counts.
#define ARRAY_SIZE 5

int main(void)
{
    int index, results[ARRAY_SIZE] = { 0 };
    float L, S;
    string input = get_string("Text: ");
    calculate_results(ARRAY_SIZE, results, input);
    // Average number of letters per 100 words in the text
    L = (100 * results[0]) / (float)results[1] ;
    // Average number of sentences per 100 words in the text
    S = (100 * results[2]) / (float)results[1] ;
    // Coleman-Liau formula
    index = round(0.0588 * L - 0.296 * S - 15.8);
    if (index > 16)
    {
        printf("Grade 16+\n");
    }
    else if (index < 1)
    {
        printf("Before Grade 1\n");
    }
    else
    {
        printf("Grade %d\n", index);
    }
}

void calculate_results(int size, int *results, const char *text)
{
    for (int i = 0; text[i] != 0; i++)
    {
        if (isalpha(text[i]))
        {
            // Calculate Letters
            results[0] ++;
        }
        else if (isspace(text[i]))
        {
            // Calculate Spaces
            results[1] ++;
        }
        else if (text[i] == '.' || text[i] == '?' || text[i] == '!')
        {
            // Calculate Sentences
            results[2] ++;
        }
    }
    // Calculate Words (Spaces + 1)
    results[1] ++;
}

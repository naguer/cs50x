// Substitution exercise | Week 2
#include <stdio.h>
#include <cs50.h>
#include <ctype.h>
#include <string.h>

int main(int argc, char *argv[])
{
    int lenghtAllowed = 26;
    // Validate arguments inputs, only accept 1 extra argument, and the lenght = 26
    if (argc != 2)
    {
        printf("Usage: ./substitution key\n");
        return 1;
    }
    else if (strlen(argv[1]) != lenghtAllowed)
    {
        printf("Key must contain %i characters.\n", lenghtAllowed);
        return 1;
    }
    // Validate if the input has all different characters
    for (int i = 0; i < lenghtAllowed; i++)
    {
        for (int j = i + 1; j < lenghtAllowed; j++)
        {
            if (argv[1][i] == argv[1][j])
            {
                printf("Key must contain %i different characters.\n", lenghtAllowed);
                return 1;
            }
        }
    }
    // Validate if the input has only letters without numbers and symbols.
    for (int i = 0; argv[1][i] != 0; i++)
    {
        if (!isalpha(argv[1][i]))
        {
            printf("Key must contain only letters.\n");
            return 1;
        }
    }
    // Ask the text to encrypt
    string plaintext = get_string("plaintext: ");

    // Use substitution cipher
    for (int i = 0; plaintext[i] != 0; i++)
    {
        if (islower(plaintext[i]))
        {
            plaintext[i] = tolower(argv[1][plaintext[i] - 'a']);
        }
        if (isupper(plaintext[i]))
        {
            plaintext[i] = toupper(argv[1][plaintext[i] - 'A']);
        }
    }
    printf("ciphertext: %s\n", plaintext);
    return 0;
}